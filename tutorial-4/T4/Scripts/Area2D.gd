extends Area2D

export (String) var sceneName = "Level 1"

func on_Area_Trigger_body_entered(body):
	print("triggered")
	if body.get_name() == "Player":
		get_tree().change_scene(str("res://Scenes/" + sceneName + ".tscn"))
	