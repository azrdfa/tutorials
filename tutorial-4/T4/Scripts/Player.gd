extends KinematicBody2D

export (int) var speed = 400
export (int) var GRAVITY = 1200
export (int) var jump_speed = -400

var count_jump = 0

const UP = Vector2(0,-1)

var velocity = Vector2()

onready var animator = self.get_node("Animator")
onready var sprite = self.get_node("Sprite")

func get_input():
	velocity.x = 0
	if count_jump < 3 and Input.is_action_just_pressed('jump'):
		if is_on_floor():
			count_jump += 1
			velocity.y = jump_speed
		else:
			if count_jump > 0:
				count_jump += 1
				velocity.y = jump_speed
	if Input.is_action_pressed('right'):
		velocity.x += speed
	if Input.is_action_pressed('left'):
		velocity.x -= speed

func _physics_process(delta):
	if is_on_floor() and count_jump > 0:
		print("fall")
		count_jump = 0
	velocity.y += delta * GRAVITY
	get_input()
	velocity = move_and_slide(velocity, UP)

func _process(delta):
	if velocity.y != 0:
		animator.play("Jump")
	elif velocity.x != 0:
		animator.play("Walk")
		if velocity.x > 0:
			sprite.flip_h = false
		else:
			sprite.flip_h = true
	else:
		animator.play("Idle")
	