extends KinematicBody2D

export (int) var speed = 400
export (int) var GRAVITY = 1200
export (int) var jump_speed = -600

const UP = Vector2(0,-1)

var velocity = Vector2()
var jump_count = 0

func _ready():
	pass

func get_input():
	velocity.x = 0
	if jump_count < 2 and Input.is_action_just_pressed('up'):
		jump_count += 1
		velocity.y = jump_speed
	if Input.is_action_pressed('right'):
		velocity.x += speed
	if Input.is_action_pressed('left'):
		velocity.x -= speed
	if Input.is_action_just_pressed('ui_select'):
		dash()
	if Input.is_action_just_released('ui_select'):
		undash()

func _physics_process(delta):
	if is_on_floor():
		jump_count = 0
	velocity.y += delta * GRAVITY
	get_input()
	velocity = move_and_slide(velocity, UP)
	
func dash():
	speed = 800

func undash():
	speed = 400
	
    
